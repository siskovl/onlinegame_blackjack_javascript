/*
   Script Created by: Sisko VL
   Date: July 7th 2017
   Last Update: Oct 15th 2017
*/

// jQuery practice and actualy worked :)  --------------------------------------
$('#deal').on('click', function() {
   blackjack.deal()
});

$('#hit').on('click', function() {
   blackjack.hit()
});

$('#stand').on('click', function() {
   blackjack.stand()
});


// Constants & variables: ------------------------------------------------------
var TITLES = ['c', 'd', 'h', 's'];
var NAMES = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K'];
var VALUES = {
   A: 1,
   2: 2,
   3: 3,
   4: 4,
   5: 5,
   6: 6,
   7: 7,
   8: 8,
   9: 9,
   T: 10,
   J: 10,
   Q: 10,
   K: 10
};

// creating a game instance
var blackjack = new Game();

// select view elements
var playerHandLabel = document.getElementById('playerHand');
var dealerHandLabel = document.getElementById('dealerHand');
var outcomeLabel = document.getElementById('outcome');
var scoreLabel = document.getElementById('score');


// Card object - stores the name and value of the card--------------------------
function Card(title, name) {
   this.title = title;
   this.name = name;
}

Card.prototype.getCard = function() {
   return this;
}

Card.prototype.getName = function() {
   return this.name;
}

Card.prototype.getTitle = function() {
   return this.title;
}


// Hand ojbect - stores the cards and the value of the hand---------------------
function Hand() {
   this.hand = [];
}

Hand.prototype.addCard = function(card) {
   this.hand.push(card);
}

Hand.prototype.getValue = function() {
// Count aces as 1, if the hand has an ace, then add 10 to value if it doesn't bust the hand
   var valueOfHand = 0;
   var handAce = false;

// Add the cards and set A to true
   for (var i = 0; i < this.hand.length; i++) {
      var name = this.hand[i].getName();
         valueOfHand += VALUES[name];
      if (name === 'A') {
         handAce = true;
      }
   }

// If we have hand without an Ace -> return the value -> else we add 10 or keep the 1 value
   if (!handAce) {
      return valueOfHand;
   }else {
      if (valueOfHand + 10 <= 21) {
         return valueOfHand +10;
      }else {
         return valueOfHand;
      }
   }
}

Hand.prototype.printHand = function() {
   var printOut = "";
   for (var i = 0; i < this.hand.length; i++) {
      printOut = printOut + "<img src=\"cards/" + this.hand[i].name + this.hand[i].title + ".png\"width=95>";
   }
   return printOut;
}

Hand.prototype.printDealerHand = function() {
   var printOut = "";
   for (var i = 0; i < this.hand.length - 1; i++) {
      printOut = printOut + '<img src="cards/facedown.jpeg", width=100>';
   }

    printOut = printOut +"<img src=\"cards/" + this.hand[this.hand.length - 1].name + this.hand[this.hand.length - 1].title + ".png\"width=95>";

   return printOut;
}


// Deck object - generates and shuffles the deck + deals the cards -------------
function Deck() {
   this.deck = new Array();

   for (var i = 0; i < TITLES.length; i++) {
      for (var j = 0; j < NAMES.length; j++) {
         var pushCard = new Card(TITLES[i], NAMES[j]);
         this.deck.push(pushCard.getCard());
      }
   }
}

// Shuffle the deck
Deck.prototype.shuffleDeck = function() {
   return shuffleArray(this.deck);
}

// Deal a card from the deck
Deck.prototype.dealCard = function() {
   var popCard = this.deck.pop();
   return popCard;
}

/*
   The game object:
   Player wins if not busted and have a hand better than dealer
   Dealer wins if not busted and have a hand equal or better than player
*/

function Game() {
   this.outcome = "";
   this.inPlay = false;
   this.score = 1000;
   this.playing_deck = [];
   this.player_hand = [];
   this.dealer_hand = [];
};

// Player is not allowed to deal new cards in hand - if he clicks on deal then he automaticly loses the points
Game.prototype.deal = function() {

   if (this.inPlay) {
      this.score -= 100;
      this.inPlay = false;
   }

   // Creating instance of hands and deck + shuffle deck
   this.inPlay = true;
   this.playing_deck = new Deck();
   this.player_hand = new Hand();
   this.dealer_hand = new Hand();
   this.playing_deck.shuffleDeck();

   // Deal the first 2 cards for player and dealer
   var roundDeal = 0;
   while (roundDeal < 2) {
      this.player_hand.addCard(this.playing_deck.dealCard())
      this.dealer_hand.addCard(this.playing_deck.dealCard())
      roundDeal += 1;
   }

   // &#x1f60e; -> SMILING FACE WITH SUNGLASSES
   if (this.dealer_hand.getValue() === 21) {
      this.outcome = "BlackJack!! - I WiN &#x1f60e; !<br /> Ready For 1 more!?";
      this.inPlay = false;

   } else {
      this.outcome = "Do you Hit or Stand?";
   }

   this.updateView();
}

// Deal another card
Game.prototype.hit = function() {
   if (this.inPlay) {

      // If players hand is over 21 -> player loses!
      this.player_hand.addCard(this.playing_deck.dealCard());

      if (this.player_hand.getValue() > 21) {
         // &#x1f631; -> FACE SCREAMING IN FEAR
         // &#x1f61c; -> FACE WITH STUCK-OUT TONGUE AND WINKING EYE
         this.outcome = "&#x1f631; BUSTED !! YOU have " + this.player_hand.getValue() + " ! <br />Do YOU know how to play? &#x1f61c; ";
         this.inPlay = false;
         this.score -= 100;

      } else {
         this.outcome = "Adding 1 Card - What now?<br /> Hit or Stand?";
      }
   }
   this.updateView();
}

// Click Stand, then it's the dealers turn
Game.prototype.stand = function() {
   if (this.inPlay) {

      // Dealer only hit cards only when he's below 17
      while (this.dealer_hand.getValue() < 17) {
         this.dealer_hand.addCard(this.playing_deck.dealCard());
      }

      // if dealers hand value is over 21, dealer busts
      if (this.dealer_hand.getValue() > 21) {
         this.outcome = "Nicely done! YOU WIN! <br /> I got " + this.dealer_hand.getValue() + " and YOU got " + this.player_hand.getValue() + "!";
         this.score += 100;

      } else {
         // if dealers hand is equal or higher to players hand, dealer wins
         if (this.dealer_hand.getValue() >= this.player_hand.getValue()) {

            this.outcome = "Opss.. YOU LOST it's " + this.player_hand.getValue() + "  vs " + this.dealer_hand.getValue() + "! Ready for Revenge ?";
            this.score -= 100;

           // else the player wins
         } else {
           this.outcome = "YaY! YOU WiN !<br/> Your score is: " + this.player_hand.getValue() + " and mine is: " + this.dealer_hand.getValue() + "!";
           this.score += 100;

         }
      }
      this.inPlay = false;
      this.updateView();

   }
}

Game.prototype.updateView = function() {
   playerHandLabel.innerHTML = this.player_hand.printHand();

   if (this.inPlay) {
      dealerHandLabel.innerHTML = this.dealer_hand.printDealerHand();
   } else {
      dealerHandLabel.innerHTML = this.dealer_hand.printHand();
   }

   outcomeLabel.innerHTML = this.outcome;
   scoreLabel.innerHTML = this.score;
}

//Suffle the array element's order
function shuffleArray(array) {
   for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
   }
   return array;
}
